﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fighter = new System.Windows.Forms.Button();
            this.strider = new System.Windows.Forms.Button();
            this.mage = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.hp = new System.Windows.Forms.TextBox();
            this.magickarcher1 = new System.Windows.Forms.TextBox();
            this.magic = new System.Windows.Forms.TextBox();
            this.magicdef = new System.Windows.Forms.TextBox();
            this.stamina = new System.Windows.Forms.TextBox();
            this.defense = new System.Windows.Forms.TextBox();
            this.attack = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.fighter1 = new System.Windows.Forms.TextBox();
            this.strider1 = new System.Windows.Forms.TextBox();
            this.mage1 = new System.Windows.Forms.TextBox();
            this.warrior1 = new System.Windows.Forms.TextBox();
            this.ranger1 = new System.Windows.Forms.TextBox();
            this.sorcerer1 = new System.Windows.Forms.TextBox();
            this.mytic1 = new System.Windows.Forms.TextBox();
            this.assassin1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.magickarcher2 = new System.Windows.Forms.TextBox();
            this.assassin2 = new System.Windows.Forms.TextBox();
            this.mystic2 = new System.Windows.Forms.TextBox();
            this.sorcerer2 = new System.Windows.Forms.TextBox();
            this.ranger2 = new System.Windows.Forms.TextBox();
            this.warrior2 = new System.Windows.Forms.TextBox();
            this.mage2 = new System.Windows.Forms.TextBox();
            this.strider2 = new System.Windows.Forms.TextBox();
            this.fighter2 = new System.Windows.Forms.TextBox();
            this.level = new System.Windows.Forms.TextBox();
            this.start = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progress = new System.Windows.Forms.Label();
            this.progress2 = new System.Windows.Forms.Label();
            this.selected = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start class";
            // 
            // fighter
            // 
            this.fighter.Location = new System.Drawing.Point(74, 155);
            this.fighter.Name = "fighter";
            this.fighter.Size = new System.Drawing.Size(75, 23);
            this.fighter.TabIndex = 1;
            this.fighter.Text = "fighter";
            this.fighter.UseVisualStyleBackColor = true;
            this.fighter.Click += new System.EventHandler(this.fighter_Click);
            // 
            // strider
            // 
            this.strider.Location = new System.Drawing.Point(155, 155);
            this.strider.Name = "strider";
            this.strider.Size = new System.Drawing.Size(75, 23);
            this.strider.TabIndex = 2;
            this.strider.Text = "strider";
            this.strider.UseVisualStyleBackColor = true;
            this.strider.Click += new System.EventHandler(this.strider_Click);
            // 
            // mage
            // 
            this.mage.Location = new System.Drawing.Point(236, 155);
            this.mage.Name = "mage";
            this.mage.Size = new System.Drawing.Size(75, 23);
            this.mage.TabIndex = 3;
            this.mage.Text = "mage";
            this.mage.UseVisualStyleBackColor = true;
            this.mage.Click += new System.EventHandler(this.mage_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Defense";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Magic";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Attack";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Stamina";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "HP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Level";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(171, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Magic defense";
            // 
            // hp
            // 
            this.hp.Location = new System.Drawing.Point(65, 45);
            this.hp.Name = "hp";
            this.hp.Size = new System.Drawing.Size(100, 20);
            this.hp.TabIndex = 11;
            // 
            // magickarcher1
            // 
            this.magickarcher1.Location = new System.Drawing.Point(94, 433);
            this.magickarcher1.Name = "magickarcher1";
            this.magickarcher1.Size = new System.Drawing.Size(100, 20);
            this.magickarcher1.TabIndex = 12;
            // 
            // magic
            // 
            this.magic.Location = new System.Drawing.Point(254, 71);
            this.magic.Name = "magic";
            this.magic.Size = new System.Drawing.Size(100, 20);
            this.magic.TabIndex = 13;
            // 
            // magicdef
            // 
            this.magicdef.Location = new System.Drawing.Point(254, 97);
            this.magicdef.Name = "magicdef";
            this.magicdef.Size = new System.Drawing.Size(100, 20);
            this.magicdef.TabIndex = 14;
            // 
            // stamina
            // 
            this.stamina.Location = new System.Drawing.Point(254, 45);
            this.stamina.Name = "stamina";
            this.stamina.Size = new System.Drawing.Size(100, 20);
            this.stamina.TabIndex = 15;
            // 
            // defense
            // 
            this.defense.Location = new System.Drawing.Point(65, 97);
            this.defense.Name = "defense";
            this.defense.Size = new System.Drawing.Size(100, 20);
            this.defense.TabIndex = 16;
            // 
            // attack
            // 
            this.attack.Location = new System.Drawing.Point(65, 71);
            this.attack.Name = "attack";
            this.attack.Size = new System.Drawing.Size(100, 20);
            this.attack.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(91, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Lvl 1 - 100";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 228);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Fighter";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 358);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Sorcerer";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 332);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Ranger";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 306);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Warrior";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 280);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Mage";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Strider";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 433);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "Magick Archer";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 410);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "Assassin";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 384);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "Mystic Knight";
            // 
            // fighter1
            // 
            this.fighter1.Location = new System.Drawing.Point(94, 225);
            this.fighter1.Name = "fighter1";
            this.fighter1.Size = new System.Drawing.Size(100, 20);
            this.fighter1.TabIndex = 28;
            // 
            // strider1
            // 
            this.strider1.Location = new System.Drawing.Point(94, 251);
            this.strider1.Name = "strider1";
            this.strider1.Size = new System.Drawing.Size(100, 20);
            this.strider1.TabIndex = 29;
            // 
            // mage1
            // 
            this.mage1.Location = new System.Drawing.Point(94, 277);
            this.mage1.Name = "mage1";
            this.mage1.Size = new System.Drawing.Size(100, 20);
            this.mage1.TabIndex = 30;
            // 
            // warrior1
            // 
            this.warrior1.Location = new System.Drawing.Point(94, 303);
            this.warrior1.Name = "warrior1";
            this.warrior1.Size = new System.Drawing.Size(100, 20);
            this.warrior1.TabIndex = 31;
            // 
            // ranger1
            // 
            this.ranger1.Location = new System.Drawing.Point(94, 329);
            this.ranger1.Name = "ranger1";
            this.ranger1.Size = new System.Drawing.Size(100, 20);
            this.ranger1.TabIndex = 32;
            // 
            // sorcerer1
            // 
            this.sorcerer1.Location = new System.Drawing.Point(94, 355);
            this.sorcerer1.Name = "sorcerer1";
            this.sorcerer1.Size = new System.Drawing.Size(100, 20);
            this.sorcerer1.TabIndex = 33;
            // 
            // mytic1
            // 
            this.mytic1.Location = new System.Drawing.Point(94, 381);
            this.mytic1.Name = "mytic1";
            this.mytic1.Size = new System.Drawing.Size(100, 20);
            this.mytic1.TabIndex = 34;
            // 
            // assassin1
            // 
            this.assassin1.Location = new System.Drawing.Point(94, 407);
            this.assassin1.Name = "assassin1";
            this.assassin1.Size = new System.Drawing.Size(100, 20);
            this.assassin1.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(205, 195);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Lvl 100 - 200";
            // 
            // magickarcher2
            // 
            this.magickarcher2.Location = new System.Drawing.Point(200, 433);
            this.magickarcher2.Name = "magickarcher2";
            this.magickarcher2.Size = new System.Drawing.Size(100, 20);
            this.magickarcher2.TabIndex = 37;
            // 
            // assassin2
            // 
            this.assassin2.Location = new System.Drawing.Point(200, 407);
            this.assassin2.Name = "assassin2";
            this.assassin2.Size = new System.Drawing.Size(100, 20);
            this.assassin2.TabIndex = 38;
            // 
            // mystic2
            // 
            this.mystic2.Location = new System.Drawing.Point(200, 381);
            this.mystic2.Name = "mystic2";
            this.mystic2.Size = new System.Drawing.Size(100, 20);
            this.mystic2.TabIndex = 39;
            // 
            // sorcerer2
            // 
            this.sorcerer2.Location = new System.Drawing.Point(200, 355);
            this.sorcerer2.Name = "sorcerer2";
            this.sorcerer2.Size = new System.Drawing.Size(100, 20);
            this.sorcerer2.TabIndex = 40;
            // 
            // ranger2
            // 
            this.ranger2.Location = new System.Drawing.Point(200, 329);
            this.ranger2.Name = "ranger2";
            this.ranger2.Size = new System.Drawing.Size(100, 20);
            this.ranger2.TabIndex = 41;
            // 
            // warrior2
            // 
            this.warrior2.Location = new System.Drawing.Point(200, 303);
            this.warrior2.Name = "warrior2";
            this.warrior2.Size = new System.Drawing.Size(100, 20);
            this.warrior2.TabIndex = 42;
            // 
            // mage2
            // 
            this.mage2.Location = new System.Drawing.Point(200, 277);
            this.mage2.Name = "mage2";
            this.mage2.Size = new System.Drawing.Size(100, 20);
            this.mage2.TabIndex = 43;
            // 
            // strider2
            // 
            this.strider2.Location = new System.Drawing.Point(200, 251);
            this.strider2.Name = "strider2";
            this.strider2.Size = new System.Drawing.Size(100, 20);
            this.strider2.TabIndex = 44;
            // 
            // fighter2
            // 
            this.fighter2.Location = new System.Drawing.Point(200, 225);
            this.fighter2.Name = "fighter2";
            this.fighter2.Size = new System.Drawing.Size(100, 20);
            this.fighter2.TabIndex = 45;
            // 
            // level
            // 
            this.level.Location = new System.Drawing.Point(65, 22);
            this.level.Name = "level";
            this.level.Size = new System.Drawing.Size(100, 20);
            this.level.TabIndex = 46;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(315, 482);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 47;
            this.start.Text = "start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // progress
            // 
            this.progress.AutoSize = true;
            this.progress.Location = new System.Drawing.Point(171, 487);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(0, 13);
            this.progress.TabIndex = 48;
            // 
            // progress2
            // 
            this.progress2.AutoSize = true;
            this.progress2.Location = new System.Drawing.Point(171, 482);
            this.progress2.Name = "progress2";
            this.progress2.Size = new System.Drawing.Size(48, 13);
            this.progress2.TabIndex = 49;
            this.progress2.Text = "Progress";
            // 
            // selected
            // 
            this.selected.Location = new System.Drawing.Point(317, 157);
            this.selected.Name = "selected";
            this.selected.Size = new System.Drawing.Size(81, 20);
            this.selected.TabIndex = 50;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 517);
            this.Controls.Add(this.selected);
            this.Controls.Add(this.progress2);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.start);
            this.Controls.Add(this.level);
            this.Controls.Add(this.fighter2);
            this.Controls.Add(this.strider2);
            this.Controls.Add(this.mage2);
            this.Controls.Add(this.warrior2);
            this.Controls.Add(this.ranger2);
            this.Controls.Add(this.sorcerer2);
            this.Controls.Add(this.mystic2);
            this.Controls.Add(this.assassin2);
            this.Controls.Add(this.magickarcher2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.assassin1);
            this.Controls.Add(this.mytic1);
            this.Controls.Add(this.sorcerer1);
            this.Controls.Add(this.ranger1);
            this.Controls.Add(this.warrior1);
            this.Controls.Add(this.mage1);
            this.Controls.Add(this.strider1);
            this.Controls.Add(this.fighter1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.attack);
            this.Controls.Add(this.defense);
            this.Controls.Add(this.stamina);
            this.Controls.Add(this.magicdef);
            this.Controls.Add(this.magic);
            this.Controls.Add(this.magickarcher1);
            this.Controls.Add(this.hp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mage);
            this.Controls.Add(this.strider);
            this.Controls.Add(this.fighter);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button fighter;
        private System.Windows.Forms.Button strider;
        private System.Windows.Forms.Button mage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox hp;
        private System.Windows.Forms.TextBox magickarcher1;
        private System.Windows.Forms.TextBox magic;
        private System.Windows.Forms.TextBox magicdef;
        private System.Windows.Forms.TextBox stamina;
        private System.Windows.Forms.TextBox defense;
        private System.Windows.Forms.TextBox attack;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox fighter1;
        private System.Windows.Forms.TextBox strider1;
        private System.Windows.Forms.TextBox mage1;
        private System.Windows.Forms.TextBox warrior1;
        private System.Windows.Forms.TextBox ranger1;
        private System.Windows.Forms.TextBox sorcerer1;
        private System.Windows.Forms.TextBox mytic1;
        private System.Windows.Forms.TextBox assassin1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox magickarcher2;
        private System.Windows.Forms.TextBox assassin2;
        private System.Windows.Forms.TextBox mystic2;
        private System.Windows.Forms.TextBox sorcerer2;
        private System.Windows.Forms.TextBox ranger2;
        private System.Windows.Forms.TextBox warrior2;
        private System.Windows.Forms.TextBox mage2;
        private System.Windows.Forms.TextBox strider2;
        private System.Windows.Forms.TextBox fighter2;
        private System.Windows.Forms.TextBox level;
        private System.Windows.Forms.Button start;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label progress;
        private System.Windows.Forms.Label progress2;
        private System.Windows.Forms.TextBox selected;
    }
}

