﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int select = 0;
        long max = 352025629371;

        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int count = 1;
            BackgroundWorker worker = sender as BackgroundWorker;

            for (int a = 0; a < 101; a++)
            {
                for (int b = 0; b < 101; b++)
                {
                    for (int c = 0; c < 101; c++)
                    {
                        for (int d = 0; d < 101; d++)
                        {
                            for (int f = 0; f < 101; f++)
                            {
                                for (int g = 0; g < 101; g++)
                                {
                                    for (int h = 0; h < 101; h++)
                                    {
                                        for (int i = 0; i < 101; i++)
                                        {
                                            for (int j = 0; j < 101; j++)
                                            {
                                                var sum = a + b + c + d + f + g + h + i + j;

                                                if (sum == 100)
                                                {
                                                    progress2.Invoke(new MethodInvoker(delegate { progress2.Text = count + "/" + max; }));
                                                }
                                                else if (sum > 100)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
                
        }

        // This event handler updates the progress.
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress2.Text = (e.ProgressPercentage.ToString() + "%");
        }

        // This event handler deals with the results of the background operation.
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progress2.Text = "Done!";
        }

        private void start_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy != true)
            {
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void fighter_Click(object sender, EventArgs e)
        {
            select = 1;
            selected.Text = "fighter";
        }

        private void strider_Click(object sender, EventArgs e)
        {
            select = 2;
            selected.Text = "strider";
        }

        private void mage_Click(object sender, EventArgs e)
        {
            select = 3;
            selected.Text = "mage";
        }
    }
}
